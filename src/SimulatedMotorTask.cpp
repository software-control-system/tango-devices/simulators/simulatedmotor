//------------------------------------------------------------------------
//
// SimulatedMotorTask.cpp: implementation of the SimulatedMotorTask class.
//
//------------------------------------------------------------------------

#include <SimulatedMotor.h>
#include "SimulatedMotorTask.h"

//-----------------------------------------------------------------------------
// SimulatedMotorTask
//-----------------------------------------------------------------------------
namespace SimulatedMotor_ns
{

SimulatedMotorTask::SimulatedMotorTask(Tango::DeviceImpl* mDevice) :
yat4tango::DeviceTask(mDevice),
m_device(mDevice)
{
    DEBUG_STREAM << "SimulatedMotorTask::SimulatedMotorTask entering ...";
    // configure optional msg handling of the mother class

    enable_timeout_msg(false);
    set_periodic_msg_period(TASK_PERIODIC_PERIOD_MS);
    enable_periodic_msg(true);
    throw_on_post_msg_timeout(false);

    m_target_position      = 0.0;
    m_current_position     = 0.0;
    m_position_increment_s = 1;

    set_state(Tango::INIT);
}


//-----------------------------------------------------------------------------
// ~SimulatedMotorTask
//-----------------------------------------------------------------------------
SimulatedMotorTask::~SimulatedMotorTask()
{
    DEBUG_STREAM << "SimulatedMotorTask::~SimulatedMotorTask" << endl;
}


//-----------------------------------------------------------------------------
// process_message
//-----------------------------------------------------------------------------
void SimulatedMotorTask::process_message(yat::Message& msg) throw (Tango::DevFailed)
{
    // handle msg
    switch (msg.type())
    {
        //-----------------------------------------------------------------------------------------------
        case yat::TASK_INIT:
        {
            try
            {
                INFO_STREAM << "__________________________________________________";
                INFO_STREAM << "SimulatedMotorTask-> yat::TASK_INIT" << endl;

                set_state(Tango::STANDBY);
            }
            catch (Tango::DevFailed& df)
            {
                on_error(df);
                throw;
            }
            INFO_STREAM << "__________________________________________________\n";
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case yat::TASK_EXIT:
        {
            INFO_STREAM << "__________________________________________________";
            INFO_STREAM << "SimulatedMotorTask-> yat::TASK_EXIT";
            set_state(Tango::INIT);

            try
            {

            }
            catch (Tango::DevFailed &df)
            {
                on_error(df);
                throw;
            }
            catch (...)
            {
                ERROR_STREAM << "Unhandled exception in SimulatedMotorTask::process_message(TASK_EXIT).";
                on_error("Task state is UNKNOWN or FAULT");
            }
            INFO_STREAM << "__________________________________________________\n";
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case yat::TASK_PERIODIC:
        {
            DEBUG_STREAM << "SimulatedMotorTask -> yat::TASK_PERIODIC";
            try
            {
                if (get_state() == Tango::MOVING)
                {
                    double delta_t = m_timer.elapsed_sec();
					m_timer.restart();
                    update_position(delta_t);
                    update_state();
                }
            }
            catch (const Tango::DevFailed& df)
            {
              ERROR_STREAM << df << std::endl;
            }
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_INITPOSITION:
        {
            yat::MutexLock lock_parameters(m_parameters_lock);
            m_target_position  = msg.get_data<double>();
            m_current_position = m_target_position;
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_SETPOSITION:
        {
            yat::MutexLock lock_parameters(m_parameters_lock);
            m_target_position = msg.get_data<double>();
			INFO_STREAM << "SimulatedMotorTask -> MSG_SETPOSITION: " << m_target_position;

            update_increment();

            set_state(Tango::MOVING);
			update_state();
			m_timer.restart();
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_STOP:
        {
            set_state(Tango::STANDBY);
        }
        break;


        //-----------------------------------------------------------------------------------------------
        case MSG_ON:
        {
            if (get_state() == Tango::OFF)
            {
                set_state(Tango::STANDBY);
            }
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_OFF:
        {
            set_state(Tango::OFF);
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_SETVELOCITY:
        {
            yat::MutexLock lock_parameters(m_parameters_lock);
            m_position_increment_s = msg.get_data<double>();
            update_increment();
        }
        break;

        //-----------------------------------------------------------------------------------------------
        case MSG_FORCESTATE:
        {
            set_state(msg.get_data<Tango::DevState>());
        }
        break;

        //-----------------------------------------------------------------------------------------------
        default:
        {
            ERROR_STREAM << "\n**************************************************";
            ERROR_STREAM << "Task yat::UNKNOWN msg !!" << endl;
            ERROR_STREAM << "**************************************************\n";
        }
        break;
    }
}


//-----------------------------------------------------------------------------
// update_position
//-----------------------------------------------------------------------------
void SimulatedMotorTask::update_increment()
{
    if (m_target_position > m_current_position)
    {
        m_position_increment_s = fabs(m_position_increment_s); // make a positive increment
    }
    else
    {
        m_position_increment_s = -fabs(m_position_increment_s); // make a negative increment
    }
    INFO_STREAM << "m_position_increment_s: " << m_position_increment_s;
}


//-----------------------------------------------------------------------------
// update_position
//-----------------------------------------------------------------------------
void SimulatedMotorTask::update_position(const double& delta_t)
{
    yat::MutexLock lock_parameters(m_parameters_lock);

    // m_position_increment_s tells how much to increment the current position for 1 second elapsed time.
    m_current_position += delta_t*m_position_increment_s;
}


//-----------------------------------------------------------------------------
// get_position
//-----------------------------------------------------------------------------
double SimulatedMotorTask::get_position()
{
    DEBUG_STREAM << "SimulatedMotorTask::get_position() entering ...";
    yat::MutexLock lock_parameters(m_parameters_lock);
    return m_current_position;
}


//-----------------------------------------------------------------------------
// update_state
//-----------------------------------------------------------------------------
void SimulatedMotorTask::update_state()
{
    yat::MutexLock lock_parameters(m_parameters_lock);

    // Check if target position is reached
    if (get_state() == Tango::MOVING)
    {
        if (   ( (m_position_increment_s > 0) && (m_current_position > m_target_position) )
			|| ( (m_position_increment_s < 0) && (m_current_position < m_target_position) )
           )
        {
            m_current_position = m_target_position;
            set_state(Tango::STANDBY);
        }
    }
}


//-----------------------------------------------------------------------------
// set_state
//-----------------------------------------------------------------------------
void SimulatedMotorTask::set_state(Tango::DevState state)
{
    {
        yat::MutexLock lock_state(m_state_lock);
        m_state = state;
    }
}


//-----------------------------------------------------------------------------
// on_error
//-----------------------------------------------------------------------------
void SimulatedMotorTask::on_error(Tango::DevFailed df)
{
    DEBUG_STREAM << "SimulatedMotorTask::on_error()" << endl;
    ERROR_STREAM << df << endl;
    stringstream status;
    status.str("");
    status << "SimulatedMotor Task is in error.\n" << endl;
    status << "Origin\t: " << df.errors[0].origin << endl;
    status << "Desc\t: " << df.errors[0].desc << endl;
    status << "Reason\t: " << df.errors[0].reason << endl;
    set_state(Tango::FAULT);
}


//-------------------------------------------------------------------
// on_error
//-------------------------------------------------------------------
void SimulatedMotorTask::on_error(std::string st)
{
    DEBUG_STREAM << "SimulatedMotorTask::on_error()"<< endl;
    string status("");
    status = "SimulatedMotor Task is in FAULT.\n"    ;
    status+=st;
    set_state(Tango::FAULT);
    set_status(status);
}


//-----------------------------------------------------------------------------
// get_state
//-----------------------------------------------------------------------------
Tango::DevState SimulatedMotorTask::get_state(void)
{
    yat::MutexLock lock_state(m_state_lock);
    return m_state;
}

//-------------------------------------------------------------------
// get_status
//-------------------------------------------------------------------
void SimulatedMotorTask::set_status(std::string status)
{
    yat::MutexLock scoped_lock(m_state_lock);
    m_status.str("");
    m_status << status.c_str() << endl;
}


//-------------------------------------------------------------------
// set_status
//-------------------------------------------------------------------
string SimulatedMotorTask::get_status(void)
{
    yat::MutexLock scoped_lock(m_state_lock);
    return (m_status.str());
}

} // SimulatedMotor_ns
