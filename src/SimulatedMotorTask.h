// SimulatedMotorTask.h: interface for the SimulatedMotorTask class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _SIMULATEDMOTORTASK_H
#define _SIMULATEDMOTORTASK_H


#include <tango.h>
#include <TangoExceptionsHelper.h>
#include <yat/threading/Mutex.h>
#include <yat/time/Timer.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>

// ============================================================================
// DEVICE TASK ACTIVITY PERIOD IN MILLISECS
// ============================================================================
// the following timeout set the frequency at which the task generates its data
#define TASK_PERIODIC_PERIOD_MS 10
#define MSG_SETPOSITION         (yat::FIRST_USER_MSG + 1000)
#define MSG_SETVELOCITY         (yat::FIRST_USER_MSG + 1001)
#define MSG_INITPOSITION        (yat::FIRST_USER_MSG + 1002)
#define MSG_STOP                (yat::FIRST_USER_MSG + 1003)
#define MSG_ON                  (yat::FIRST_USER_MSG + 1004)
#define MSG_OFF                 (yat::FIRST_USER_MSG + 1005)
#define MSG_FORCESTATE          (yat::FIRST_USER_MSG + 1006)

namespace SimulatedMotor_ns
{

class SimulatedMotorTask : public yat4tango::DeviceTask
{
public:
    // ctor
    SimulatedMotorTask(Tango::DeviceImpl* mDevice);
    // dtor
    virtual ~SimulatedMotorTask();

    ///@return the last state of the task
    Tango::DevState get_state(void);

    ///@return current status message
    string get_status(void);

    ///@return the current position value
    double get_position();

protected:// [yat4tango::DeviceTask implementation]
    virtual void process_message(yat::Message& msg) throw (Tango::DevFailed);

private:
    void set_state(Tango::DevState state);

    /// set state to Tango::FAULT & fix the status of the error
    void on_error(Tango::DevFailed df);
    void on_error(std::string st);
    void set_status(std::string status);

    void update_position(const double& delta_t);
    void update_state();
    void update_increment();

// Privates members
    /// owner device server object
    Tango::DeviceImpl* m_device;

    yat::Timer m_timer;
    yat::Mutex m_state_lock;
    yat::Mutex m_parameters_lock;
    Tango::DevState m_state;
    stringstream m_status;

    double m_target_position;
    double m_current_position;
    double m_position_increment_s;

    double m_last_timer;

    bool   m_is_forced_state;
    Tango::DevState m_forced_state;
};


//-------------------------------------------------------------
// Functor called when reset() SimulatedMotorTask
struct TaskExiter
{
    void operator ()(yat4tango::DeviceTask * t)
    {
        try
        {
            cout << "TaskExiter..." << endl;
            t->exit();
        }
        catch (...)
        {
        }
    }
};

}

#endif // _SIMULATEDMOTORTASK_H
